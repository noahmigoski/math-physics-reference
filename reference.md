---
title: "Math/Physics Reference"
author: [Noah Migoski]
date: "2021-03-09"
keywords: [Math, Physics]
titlepage: "true"
titlepage-rule-color: "458588"
titlepage-text-color: "3c3836"

...


\tableofcontents

\newpage

Math
====


- Power Rule

    $$ \frac{d}{dx} x^n = n x^{n - 1}; x \in \mathbb{C}, n \in \mathbb{R} $$ 
- Quadratic Formula:

    $$ ax^2 + bx + c = 0 \implies x = \frac{-b \pm \sqrt{b^2 - 4ac}}{2a} $$

Complex Analysis
----------------

### Definitions:

- Analytic Function: A function is analytic on an open set if it has a derivative everywhere in that set. It is analytic at a point if it is analyitic in some neighborhood of that point.

- Entire: An entire function is analytic on all of $\mathbb{C}$.

### Formulas

- Euler's Identity/Polar Decomposition

    $$ z = re^{i \theta} = rcos(\theta) + i r sin(\theta); \theta, r \in \mathbb{R}, z \in \mathbb{C} $$

    where

    $$ r = |z|, \theta = arctan(Im(z)/Re(z)) = arg(z) $$

- Multiplicative Inverse

    $$ 1/z = \bar{z}/|z|^2 = \frac{e^{-i\theta}}{r} $$

- Cauchy-Riemann Equations

    Let $f(z) = u(x,y) + iv(x,y)$, f(z) is differentiable iff,
    
    \begin{equation} u_x = v_y, u_y = -v_x \end{equation}
    
    In which case $f'(z) = u_x + iv_x = v_y - iu_y$ 
    
    Equivalently if $f(z) = u(r,\theta) + iv(r, \theta)$

    $$ ru_r = v_\theta, u_\theta = -rv_r \implies f'(z) = e^{-i\theta}(u_r + iv_r) $$

    where the subscripts indicate partial derivatives.

- Countours

    Perameterize a countour by t

    $$ c(t) = \{z = z(t) :a \leq t \leq b \} $$

    Then the length of c is

    $$ L = \int_a^b |z'(t)| dt $$

    For an arbitrary function evaluated along the contour

    $$ \int_c f(z) dz = \int_a^b f(z(t))z'(t) dz $$

    Which can be estimated by

    $$ |\int_c f(z) dz | \leq ML $$

    where $M = max(f(z)) \text{ for } z \in c$.

    We can also estimate that

    $$ |\int_a^b f(t) dt| \leq \int_a^b |f(t)| dt $$

    If $f(z)$ is analytic on the interior of a simple closed countour then

    $$ \oint f(z) dz = 0 $$

    
- Cauchy Integral Formula

    \begin{equation} f^{(n)}(z_0) = \frac{n!}{2\pi i} \int_c \frac{f(z)}{(z-z_0)^{n+1}} dz \end{equation}

    Where
    
    $$n \geq 0 \in \mathbb{Z}, c = b\mathcal{D}, z_0 \in \mathcal{D} $$

Physics
=======

Quantum
-------

### Time-Independent Perturbation Theory:
    
- Non-Degenerate case

    $$ H(\lambda) = H_0 + \lambda V $$

    Where $\lambda \in [0, 1]$ is a continuous parameter.

    If the eigenstates and eigenvalues are analytic in lambda, we can write:

    $$ \overline{G_0}(z) := (z-H_0)^{-1} (\mathbb{I} - |n\rangle \langle n|) $$

    $$ |n(\lambda)\rangle = \overline{G_0}(E_n) \lambda V |n(\lambda) \rangle $$

    Where $E_n$ is an eigenvalue of the unperturbed Hamiltonian $H_0$.

    Iteration gives:

    $$ |n^1 \rangle = \overline{G_0}(E_n)V| n \rangle = \sum_{n \neq m} \frac{\langle m | V | n \rangle}{E_n - E_m} | m \rangle $$

    $$ |n^2 \rangle = \overline{G_0}(E_n) V | n^1 \rangle - G_0(E_n)|n^1 \rangle \langle n | V | n \rangle $$
    
    Equivalently:

    $$ |n^2 \rangle = \sum_{m \neq n} \left( \sum_{m' \neq n} \frac{V_{mm'}V_{m'n}}{(E_n - E_m)(E_n - E_{m'})} \right) | m \rangle - \sum_{m \neq n} \frac{V_{mn} V_{nn}}{(E_n - E_m)^2} | m \rangle $$

    With Energies:

    $$ E_n^1(\lambda) = \langle n | V | n \rangle = V_{nn} $$

    $$ E_n^2(\lambda) = \langle n | V | n^1 \rangle = \sum_{m \neq n} \frac{|V_{mn}|^2}{E_n - E_m} $$

    $$ E_n^k(\lambda) = \langle n | V | n^{k-1} \rangle $$

    Notice that kets must be renormalized, this can be done as follows:

    $$ | \bar n(\lambda) \rangle = Z_n^{1/2} | n(\lambda) \rangle $$

    Where $Z_n$ is the probability for the state to remain in $|n \rangle$.

    $$ Z_n \approx 1 - \lambda^2 \sum_{m \neq n} \frac{|V_{mn}|^2}{(E_n - E_m)^2} $$

- Degenerate Case

    $$ H(\lambda) = H_0 + \lambda V : H_0 | n_j \rangle = E_n | n_j \rangle ; 0<j<N_g \in \mathbb{Z} $$

    $$ | \psi^{(1)} \rangle = \sum_{m \neq \mathcal{M}_n} \left[ \sum_{j = 1}^{N_g} \alpha_j \frac{\langle m | V | n \rangle}{E_n - E_m} \right] | m \rangle $$

    Where

    $$ \mathcal{M}_n = \{|n_j \rangle \}_{j=1,2,...,N_g} $$
    
    and

    $$ | \psi^{(0)} \rangle = \sum_{j = 1}^{N_g} \alpha_j | n_j \rangle $$

### Time-Dependent Perturbation Theory:

- Interaction Picture

    $$ H(t) = H_0 + V(t) $$

    $$ |\psi(t)\rangle_I = U_I(t, t_0) |\psi(t_0)\rangle_I $$

    TODO: Include energy equation too
    
    Where:

    $$ V_I = e^{i/\hbar H_0 t}Ve^{-i/\hbar H_0} $$

